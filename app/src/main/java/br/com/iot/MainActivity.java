package br.com.iot;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference databaseReference;
    private IoT ioT = new IoT();
    private TextView txtChave1, txtChave2, txtChave3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtChave1 = (TextView) findViewById(R.id.chave1_id);
        txtChave2 = (TextView) findViewById(R.id.chave2_id);
        txtChave3 = (TextView) findViewById(R.id.chave3_id);

        databaseReference = FirebaseDatabase.getInstance().getReference("IoT");


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    ioT = dataSnapshot.getValue(IoT.class);

                    if (ioT == null) {
                        ioT = new IoT();
                    }

                    txtChave1.setText("Chave 1: "+String.valueOf(ioT.estados.chave1));
                    txtChave2.setText("Chave 2: "+String.valueOf(ioT.estados.chave2));
                    txtChave3.setText("Chave 3: "+String.valueOf(ioT.estados.chave3));


                }catch (Exception e) {
                    Log.e("IOT", e.getMessage(), e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void ligaDesligaLed1(View view) {
        ioT.estados.led1 = !ioT.estados.led1;
        databaseReference.setValue(ioT);
    }

    public void ligaDesligaLed2(View view) {
        ioT.estados.led2 = !ioT.estados.led2;
        databaseReference.setValue(ioT);
    }

    public void ligaDesligaLed3(View view) {
        ioT.estados.led3 = !ioT.estados.led3;
        databaseReference.setValue(ioT);
    }
}
